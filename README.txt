  Banlist module for Drupal
====================================================


Description
===========
Banlist allows the administrator to disallow users from using a
username from a list of usernames. The list consist of usernames such
as: admin, administrator, root, demo, test, superuser, etc.

Although core can handle this, the primary purpose of the module is for people
in the community to contribute to the list and make it very easy for everyone
else to take advantage.


Installation
=============

1. Unpack the Banlist zip contents in the appropriate modules directory of your 
Drupal installation.  This is probably sites/all/modules/

2. Enable the Banlist module in the administration tools.

3. If you're not using Drupal's default administrative account, make sure 
"administer ban list" is enabled through access control administration.


Configuration
=============
Once the module is activated, go to:

Administer >> User management >> Access rules >> Ban list (admin/user/rules/banlist).

Here you will be able to view, add, and delete usernames from the list.


Current Maintainer
==================
* Yonas Yanfa (http://drupal.org/user/473174)
