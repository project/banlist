<?php

/**
 * Implementation of hook_menu().
 */
function banlist_menu() 
{
  $items['admin/user/rules/banlist'] = array(
    'title' => 'Ban list',
    'page callback' => drupal_get_form,
    'page arguments' => array('banlist_form'),
    'access callback' => 'banlist_access_callback',
    'access arguments' => array('administer ban list'),
    'type' => MENU_LOCAL_TASK,
    'weight'           => 10,
    );

  return $items;
}

/**
 * Implementation of hook_perm().
 */
function banlist_perm() {
  return array('administer ban list');
}

/**
 * Access callback for banlist_menu().
 *
 * @param string $permission
 * The permission that is required.
 *
 * @param array $account
 * Optional. The account that the current user must have. 
 *
 * @return boolean
 * TRUE if access is granted. FALSE if access is denied.
 *
 */
function banlist_access_callback($permission, $account = NULL) 
{
  if (!isset($account)) {
      return user_access($permission);
  }

  global $user;

  return ($account->uid == $user->uid && user_access($permission));
}

/**
 * Administration form to control which usernames to ban.
 */
function banlist_form()
{
  static $list = array('admin', 'administrator', 'help', 'helpdesk', 'op', 'oper', 'operator', 'root', 'superadmin', 'superuser', 'toor');

  $disabled = banlist_get_missing_masks($list, 'user');
  $enabled = array_diff($list, $disabled);

  $form['banlist_usernames'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Usernames'),
      '#description' => t('Users will not be able to use or create an account with a username in this list.'),
      '#options' => drupal_map_assoc(array_merge($enabled, $disabled)),
      '#default_value' => $enabled,
      );
  $form['banlist_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      );
  return $form;
}

/**
 * Implementation of banlist_form submit handler.
 */
function banlist_form_submit(&$form, &$form_state)
{
  $status = banlist_get_checkbox_status($form_state['values']['banlist_usernames']);

  banlist_enable_masks(array('user' => $status['enabled']));
  banlist_disable_masks(array('user' => $status['disabled']));
}

/**
 * Determine which checkboxes where enabled/disabled.
 */
function banlist_get_checkbox_status(&$choices)
{
  $enabled = array();
  $disabled = array();

  foreach ($choices as $name => $value)
  {
    if ($value !== 0)  
      $enabled[] = $name; 
    else
      $disabled[] = $name; 
  }
  return array('enabled' => $enabled, 'disabled' => $disabled);
}

/**
 * Enable/create an array of masks.
 *
 * @param array $masks
 * The array of arrays, where each subarray is name of the mask type . Only 'user' is supported right now.
 *
 */
function banlist_enable_masks($masks)
{
  if (isset($masks) && isset($masks['user']))
    banlist_create_masks( banlist_get_missing_masks($masks['user'], 'user'), 'user' );
}

/**
 * Delete an array of masks.
 *
 * @param array $masks
 * The array of arrays, where each subarray is name of the mask type . Only 'user' is supported right now.
 *
 */
function banlist_disable_masks($masks)
{
  if (isset($masks) && isset($masks['user']))
    banlist_remove_masks( $masks['user'], 'user' );
}

/**
 * Create an array of masks.
 *
 * @param array $masks
 * The array of masks to create.
 *
 * @param string $type
 * The type of mask to create for each value of $masks.
 *
 */
function banlist_create_masks($masks, $type)
{
  foreach ($masks as $mask) {
    db_query("INSERT INTO {access} (mask, type, status) VALUES ('%s', '%s', %d)", $mask, $type, 0);
  }
}

/**
 * Delete an array of masks.
 *
 * @param array $masks
 * The array of masks to delete.
 *
 * @param string $type
 * The type of mask to delete for each value of $masks.
 *
 */
function banlist_remove_masks($masks, $type)
{
  foreach ($masks as $mask) {
    db_query("DELETE FROM {access} WHERE mask = '%s' AND type = '%s' AND status = %d", $mask, $type, 0);
  }
}

/**
 * Determine which masks are missing from the database for a given array of masks.
 *
 * @param array $masks
 * The array of masks to search for.
 *
 * @param string $type
 * The type of mask for each value of $masks.
 *
 * @return array
 * An array of masks that are in $masks, but not in the database.
 *
 */
function banlist_get_missing_masks($masks, $type)
{
  if (!isset($masks) || empty($masks))
    return array();

  $placeholders = db_placeholders($masks, 'varchar');
  $args = array_merge(array($type), $masks);

  // now convert $masks into an associative array
  $masks = drupal_map_assoc($masks);

  // did we find all the masks?
  $count = db_result(db_query("SELECT COUNT(DISTINCT mask) FROM {access} WHERE type = '%s' AND mask IN (". $placeholders .")", $args));
  if ($count == count($masks))
  {
    return array();
  }

  $result = db_query("SELECT DISTINCT mask FROM {access} WHERE type = '%s' AND mask IN (". $placeholders .")", $args);

  // remove the masks that we have found
  while($row = db_fetch_object($result)) {
    unset($masks[$row->mask]);
  }

  return array_keys($masks);
}

